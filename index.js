const express = require('express');
const cors = require('cors');
const app = express();
const {join} = require('path');
const PORT = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());

app.use(express.static(join(__dirname,'client')));
app.use('/form',express.static(join(__dirname,'client','form')))
app.use('/admin',express.static(join(__dirname,'client','admin')))



app.post('/login',(req,res)=>{
  // let reqObj = {...req.body,sessionId}
  if(req.body.pass==="vCouncilRocks" && req.body.user==="council") res.status(200).send({message:"Logged in!"});
  else res.status(401).send({message:"unauthorized"});
})

app.listen(PORT,()=>console.log('Server started'))