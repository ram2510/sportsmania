
document.getElementById('sports').value = sessionStorage.getItem('item')

const checkSportsAvail=async ()=>{
  let data = await fetch('https://backendverve.azurewebsites.net/api/sportsmania/getTeamCountBySports',{
    method:"POST",
    headers:{
      "Content-Type":"application/json"
    },
    body:JSON.stringify({sports:sessionStorage.getItem('item')})
  }).then(res=>{
    if(res.status===200)
      return res.json();
    
      alert("We are experiencing issues please contact the vit sports council");
  })
  
  if(data.count>=12){
    alert(`The registrations for ${sessionStorage.getItem('item')} is done please contact VIT sports council to book a seat now! The pay button is disabled`);
    document.getElementById("pay").disabled = true;
  }
}

checkSportsAvail();


let optionData = [{name:'Ring Football (Boys)',entry:600,token:100},{name:"Ring Football (Girls)",entry:300,token:100},{name:"Long Football",entry:1400,token:400},{name:"Basketball (Boys)",entry:500,token:200},{name:"Basketball (Girls)",entry:300,token:100},{name:"Volleyball (Girls)",entry:600,token:100},{name:"Volleyball (Boys)",entry:800,token:300},{name:"Throwball",entry:700,token:200},{name:"Kabbadi",entry:700,token:200},{name:"Chess",entry:120,token:50},{name:"Carrom (single)",entry:100,token:50},{name:"Carrom (doubles)",entry:200,token:50},{name:"Table Tenis (single)",entry:100,token:50},{name:"Table Tennis (double)",entry:200,token:50}].find(item=>item.name===sessionStorage.getItem('item'));
let isSportsAvail=true;
let select = document.getElementById("payment");
let optionToken = document.createElement('option');
let optionEntry = document.createElement('option');
optionEntry.value = optionData.entry;
optionToken.value = optionData.token;
optionToken.innerHTML = `&#8377;${optionData.token} (token)`;
optionEntry.innerHTML=`&#8377;${optionData.entry} (entry)`
select.add(optionToken,null);
select.add(optionEntry,null)
function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

/**
 * @param {string} paymentSite - Payment site.
 * @param {number} amount - Price of event.
 * @param {string} callback - Return link to caller website (mandatory to be a URI).
 * @param {string} eventType - Type of event.
 * @param {srting} id - Event Identifier.
 */
function pay(paymentSite, amount, callback, eventType, id) {
  window.location.href = paymentSite 
                      + amount + "/" 
                      + eventType + "/" 
                      + id + "/"
                      + encodeURIComponent(callback);
}

const onsubmit=async ()=>{
  let allValueValid=true;
  let reqObj={}
  document.querySelectorAll('.form__field').forEach(item=>{

    if(!item.value.trim().length && item.id !== 'c3') allValueValid = false;

    if(item.id === 'c1' || item.id === 'c2')
      if(item.value.trim().length!==10) allValueValid=false;
    
    
    reqObj[item.id] = item.value.trim();
  })


  if(!allValueValid) {
    alert("Enter all the required details")
    return
  }

  const payment = document.getElementById('payment').value

  const uuid = uuidv4();

  // console.log(reqObj)
  
  const addTeamUrl = "https://backendverve.azurewebsites.net/api/sportsmania/addTeam";

  reqObj.teamID = uuid;
  
  const data = await fetch(addTeamUrl,{
    method:"POST",
    headers:{
      "Content-Type":"application/json"
    },
    body:JSON.stringify(reqObj)
  }).then(res=>res.json())

  pay(data.redirectURL,payment,'http://localhost:5000/','sports',uuid)
}

document.getElementById('container').addEventListener('click',onsubmit)